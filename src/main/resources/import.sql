INSERT INTO `country` (`country_id`, `country`) VALUES (1, 'France');
INSERT INTO `country` (`country_id`, `country`) VALUES (2, 'Belgique');
INSERT INTO `country` (`country_id`, `country`) VALUES (3, 'Pays-Bas');

INSERT INTO `town` (`town_id`, `country_country_id`, `city_code`, `city`) VALUES (1, 1, '75015', 'Paris');
INSERT INTO `town` (`town_id`, `country_country_id`, `city_code`, `city`) VALUES (2, 1, '78530', 'Buc');
INSERT INTO `town` (`town_id`, `country_country_id`, `city_code`, `city`) VALUES (3, 1, '92800', 'Puteaux');
INSERT INTO `town` (`town_id`, `country_country_id`,  `city`) VALUES (4, 3,  'Vandermonde');

INSERT INTO `location` (`location_id`,`town_town_id`, `stadium_name`, `adress`) VALUES (1, 1, 'Suzanne Lenglen', 'Balard');
INSERT INTO `location` (`location_id`,`town_town_id`, `stadium_name`, `adress`) VALUES (2, 3, 'Ile de puteaux', 'Ile de puteaux');

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `birthdate`, `phone_number`, `mail`,`adress`,`password`,`date_of_certificate`,`date_of_validation`,`date_of_refusal`) VALUES (1, 'jp', 'marque', '1987-03-27', '0165487818', 'jp@marque','11 rue truillot','password','2023-01-12', '2022-01-14',null);
INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `birthdate`, `phone_number`, `mail`,`adress`,`password`,`date_of_certificate`,`date_of_validation`,`date_of_refusal`) VALUES (2, 'nico', 'etasse', '1999-1-10', '0165487817', 'nico@etasse','3 avenue Legrand','password','2023-01-13', '2022-01-17',null);
INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `birthdate`, `phone_number`, `mail`,`adress`, `password`,`date_of_certificate`,`date_of_validation`,`date_of_refusal`) VALUES (3, 'louis', 'dumeau', '1995-2-25', '0165487845', 'louis@dumeau','4 boulevard Raspail', '$2y$10$a.JK2zbXKstntVSjLvfsj.eG9n4KWBqX99apOjvDe4MDOzYl3YUpy','2023-01-21', '2021-01-23',null);
INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `birthdate`, `phone_number`, `mail`,`adress`,`password`,`date_of_certificate`,`date_of_validation`,`date_of_refusal`) VALUES (4, 'fabrice', 'blabla', '1997-3-3', '0689857899', 'fabrice@blabla','56 rue Edgar Quinet','password','2023-03-25', '2021-03-30',null);
INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `birthdate`, `phone_number`, `mail`,`adress`,`password`,`date_of_certificate`,`date_of_validation`,`date_of_refusal`) VALUES (5, 'michelle', 'katoiz', '1992-4-7', '0615412301', 'michelle@katoiz','9 avenue Gigachad','password','2023-07-19', '2021-08-23',null);
INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `birthdate`, `phone_number`, `mail`,`adress`,`password`,`date_of_certificate`,`date_of_validation`,`date_of_refusal`) VALUES (6, 'raphaelle', 'bernard', '1990-5-19', '0659021636', 'raphaelle@bernard',null,'password',null,null,'2021-10-05');
INSERT INTO `user` (`first_name`, `last_name`, `birthdate`, `phone_number`, `mail`,`password`) VALUES ('Vilain','refusable', '1992-12-12','0612587935','vilain@refuse','$2y$10$a.JK2zbXKstntVSjLvfsj.eG9n4KWBqX99apOjvDe4MDOzYl3YUpy');
INSERT INTO `user` (`first_name`, `last_name`, `birthdate`, `phone_number`, `mail`,`password`) VALUES ('Jonas','Guerche', '1990-6-1','0612587935','jonas@guerche','$2y$10$a.JK2zbXKstntVSjLvfsj.eG9n4KWBqX99apOjvDe4MDOzYl3YUpy');
INSERT INTO `user` (`first_name`, `last_name`, `birthdate`, `phone_number`, `mail`,`password`) VALUES ('Gabriel','Guerche', '1990-6-1','0612587935','gabi@guerche','$2y$10$a.JK2zbXKstntVSjLvfsj.eG9n4KWBqX99apOjvDe4MDOzYl3YUpy');
INSERT INTO `user` (`first_name`, `last_name`, `birthdate`, `phone_number`, `mail`,`password`) VALUES ('Vilain','espion', '1991-7-20','0612587935','vilain@espion','$2y$10$a.JK2zbXKstntVSjLvfsj.eG9n4KWBqX99apOjvDe4MDOzYl3YUpy');



INSERT INTO `role` (`role_id`,`role`) VALUES (1, 'PLAYER');
INSERT INTO `role` (`role_id`,`role`) VALUES (2, 'ADMIN');

INSERT INTO `role_person` (`user_id`,`role_id`) VALUES (1, 1);
INSERT INTO `role_person` (`user_id`,`role_id`) VALUES (2, 1);
INSERT INTO `role_person` (`user_id`,`role_id`) VALUES (3, 2);
INSERT INTO `role_person` (`user_id`,`role_id`) VALUES (4, 1);
INSERT INTO `role_person` (`user_id`,`role_id`) VALUES (5, 2);


INSERT INTO `season` (`season_id`, `season_beginning`, `season_end`) VALUES (1, '2021-8-1', '2022-7-31');
INSERT INTO `season` (`season_id`, `season_beginning`, `season_end`) VALUES (2, '2022-8-1', '2023-7-31');

INSERT INTO `licence` (`licence_id`,`season_season_id`,`user_user_id`, `written_date`, `amount`) VALUES (1, 1, 1, '2021-9-22', '70');
INSERT INTO `licence` (`licence_id`,`season_season_id`,`user_user_id`, `written_date`, `amount`) VALUES (2, 2, 1, '2022-8-12', '70');
INSERT INTO `licence` (`licence_id`,`season_season_id`,`user_user_id`, `written_date`, `amount`) VALUES (3, 1, 2, '2021-1-24', '80');
INSERT INTO `licence` (`licence_id`,`season_season_id`,`user_user_id`, `written_date`, `amount`) VALUES (4, 2, 2, '2022-9-12', '80');
INSERT INTO `licence` (`licence_id`,`season_season_id`,`user_user_id`, `written_date`, `amount`) VALUES (5, 1, 3, '2021-1-20', '80');
INSERT INTO `licence` (`licence_id`,`season_season_id`,`user_user_id`, `written_date`, `amount`) VALUES (6, 2, 3, '2022-10-22', '80');
INSERT INTO `licence` (`licence_id`,`season_season_id`,`user_user_id`, `written_date`, `amount`) VALUES (7, 1, 4, '2021-1-17', '80');
INSERT INTO `licence` (`licence_id`,`season_season_id`,`user_user_id`, `written_date`, `amount`) VALUES (8, 2, 4, '2022-8-10', '80');
INSERT INTO `licence` (`licence_id`,`season_season_id`,`user_user_id`, `written_date`, `amount`) VALUES (9, 1, 5, '2021-10-17', '80');

INSERT INTO `team` (`team_id`,`team_name`) VALUES (1, 'kookies');
INSERT INTO `team` (`team_id`,`team_name`) VALUES (2, 'ours');
INSERT INTO `team` (`team_id`,`team_name`) VALUES (3, 'duc');
INSERT INTO `team` (`team_id`,`team_name`) VALUES (4, 'loulous');

INSERT INTO `event` (`event_id`,`location_location_id`, `previsionnal_start_date`, `effective_start_date`, `event_type`) VALUES (1, 1, '2022-9-17', '2022-9-17','rugby_match');
INSERT INTO `event` (`event_id`,`location_location_id`, `previsionnal_start_date`, `effective_start_date`, `event_type`) VALUES (2, 2, '2022-9-24', '2022-9-24','rugby_match');
INSERT INTO `event` (`event_id`,`location_location_id`, `previsionnal_start_date`, `effective_start_date`, `event_type`) VALUES (3, 2, '2022-10-1', '2022-10-1','rugby_match');
INSERT INTO `event` (`event_id`,`location_location_id`, `previsionnal_start_date`, `effective_start_date`, `event_type`) VALUES (4, 1, '2022-10-8', '2022-10-15','rugby_match');
INSERT INTO `event` (`event_id`,`location_location_id`, `previsionnal_start_date`, `effective_start_date`, `event_type`) VALUES (5, 1, '2022-11-10', null,'particular_event');



INSERT INTO `rugby_match` (`event_id`,`season_season_id`,`team_team_id`, `user_user_id`, `point_scored`,`point_taken`,`bonus_point`,`payment_referee`,`payment_field`) VALUES (1, 1, 1, 3, 10, 20, 0, 70, 100);
INSERT INTO `rugby_match` (`event_id`,`season_season_id`,`team_team_id`, `user_user_id`, `point_scored`,`point_taken`,`bonus_point`,`payment_referee`,`payment_field`) VALUES (2, 2, 2, 3, 10, 0, 0, 70, 100);
INSERT INTO `rugby_match` (`event_id`,`season_season_id`,`team_team_id`, `user_user_id`, `point_scored`,`point_taken`,`bonus_point`,`payment_referee`,`payment_field`) VALUES (3, 2, 3, 3, 10, 12, 1, 70, 100);
INSERT INTO `rugby_match` (`event_id`,`season_season_id`,`team_team_id`, `user_user_id`, `point_scored`,`point_taken`,`bonus_point`,`payment_referee`,`payment_field`) VALUES (4, 2, 4, 3, 27, 10, 1, 80, 100);

INSERT INTO `particular_event` (`event_id`,`previsionnal_end_date`,`effective_end_date`, `price`, `event_description`, `event_name`) VALUES (5, '2022-11-14',null, 2000, 'Flanders', 'Tournoi Rugby');

-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (1, 1);
-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (1, 2);
-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (1, 3);
-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (2, 1);
-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (2, 2);
-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (2, 3);
-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (1, 4);
-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (2, 4);
-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (5, 1);
-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (5, 2);
-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (3, 3);
-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (3, 4);
-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (4, 1);
-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (4, 2);
-- INSERT INTO `presence` (`user_id`,`event_id`) VALUES (4, 3);


INSERT INTO `expense` (`expense_id`,`event_event_id`, `date_expense`, `amount`, `reason`) VALUES (1, null, '2022-11-14',300, 'Achat de maillots');


