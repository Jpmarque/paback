package fr.eql.ai112.p3JP.PAback.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "location")
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "location_id")
    private Long locationId;

    private String adress;

    @Column(name = "stadium_name")
    private String stadiumName;

    @ManyToOne
    @JoinColumn(referencedColumnName = "town_id")
    private Town town;

    @OneToMany(mappedBy = "location", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private final List<Event> events = new ArrayList<>();
}
