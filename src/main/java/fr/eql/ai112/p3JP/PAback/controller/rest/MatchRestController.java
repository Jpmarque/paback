package fr.eql.ai112.p3JP.PAback.controller.rest;

import fr.eql.ai112.p3JP.PAback.entity.dto.PlayerDto;
import fr.eql.ai112.p3JP.PAback.entity.dto.PresenceDto;
import fr.eql.ai112.p3JP.PAback.entity.dto.ThisSeasonMatchDto;
import fr.eql.ai112.p3JP.PAback.service.MatchService;
import fr.eql.ai112.p3JP.PAback.utils.UrlUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = UrlUtils.FRONT_END_URL)
@RequestMapping("/matches")
public class MatchRestController {

    MatchService matchService;

    @Autowired
    public void setMatchService(MatchService matchService) {
        this.matchService = matchService;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/players")
    List<PlayerDto> getALLPlayerLicenced(){
        return matchService.getALLPlayerLicenced();
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("")
    List<ThisSeasonMatchDto> matchesThisSeason(){
        return matchService.matchesThisSeason();
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/validate")
    public void validatePlayer(@RequestBody PresenceDto presenceDto) {
        matchService.saveParticipation(presenceDto);
    }
//    @PreAuthorize("hasAuthority('ADMIN')")
//    @PostMapping("/validate")
//    public User validatePlayer(@RequestBody User user, Event event) {
//        User userbis = matchService.findByUserId(user.getUserId());
//        RugbyMatch rugbyMatch = matchService.findByEventId(event.getEventId());
//        userbis.addRugbyPresence(rugbyMatch);
//
//        return matchService.save(user);
//    }
}
