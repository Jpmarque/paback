package fr.eql.ai112.p3JP.PAback.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Licence {

    private float amount;

    @Column(name = "written_date")
    private LocalDate writtenDate;


    @ManyToOne
    @JoinColumn(referencedColumnName = "user_id")
    private User user;


    @ManyToOne
    @JoinColumn(referencedColumnName = "season_id")
    private Season season;

    @Id
    private Long licence_id;

}
