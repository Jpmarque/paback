package fr.eql.ai112.p3JP.PAback.service.impl;

import fr.eql.ai112.p3JP.PAback.entity.Role;
import fr.eql.ai112.p3JP.PAback.entity.User;
import fr.eql.ai112.p3JP.PAback.entity.dto.MemberDto;
import fr.eql.ai112.p3JP.PAback.repository.MemberDao;
import fr.eql.ai112.p3JP.PAback.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class MemberServiceImpl implements MemberService {

    MemberDao memberDao;

    public MemberDto getMember(long id) {
        byte gamesPlayedThisSeason = memberDao.gamesPlayedThisSeason(id);
        LocalDate thisYearLicenceDate = memberDao.thisYearLicenceDate(id);
        User findByUserId = memberDao.findByUserId(id);

        MemberDto memberDto = new MemberDto((List<Role>) findByUserId.getAuthorities(), findByUserId.getMail(), findByUserId.getPassword(), findByUserId.getFirstName(), findByUserId.getLastName(), findByUserId.getPhoneNumber(),
                findByUserId.getBirthdate(), findByUserId.getDateOfValidation(), findByUserId.getDateOfRefusal(), findByUserId.getAdress(), gamesPlayedThisSeason, findByUserId.getDateOfCertificate(), thisYearLicenceDate);

        System.out.println("memberDto crée");
        System.out.println(memberDto);
        return memberDto;
    }


    public List<MemberDto> getAllMembers(){
        List<MemberDto> getAllMembers = new ArrayList<>();
        long numberOfUser=memberDao.numberOfUser();
        for(int id=1; id<=numberOfUser; id++){
            byte gamesPlayedThisSeason = memberDao.gamesPlayedThisSeason(id);
            LocalDate thisYearLicenceDate = memberDao.thisYearLicenceDate(id);
            User findByUserId = memberDao.findByUserId(id);
            MemberDto memberDto = new MemberDto((List<Role>) findByUserId.getAuthorities(), findByUserId.getMail(), findByUserId.getPassword(),
                    findByUserId.getFirstName(), findByUserId.getLastName(), findByUserId.getPhoneNumber(), findByUserId.getBirthdate(),
                    findByUserId.getDateOfValidation(), findByUserId.getDateOfRefusal(), findByUserId.getAdress(), gamesPlayedThisSeason,
                    findByUserId.getDateOfCertificate(), thisYearLicenceDate);
            getAllMembers.add(memberDto);
        }
        return getAllMembers;
    }




    @Autowired
    public void setMemberDao(MemberDao memberDao) {
        this.memberDao = memberDao;
    }
}
