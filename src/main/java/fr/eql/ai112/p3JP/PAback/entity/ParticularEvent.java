package fr.eql.ai112.p3JP.PAback.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.DiscriminatorValue;

@Entity
@DiscriminatorValue("P")
@Table(name = "particular_event")
public class ParticularEvent extends Event {

    @Column(name = "previsionnal_end_date")
    private LocalDate previsionnalEndDate;

    @Column(name = "effective_end_date")
    private LocalDate effectiveEndDate;

    private Float price;

    @Column(name = "event_name")
    private String eventName;

    @Column(name = "event_description")
    private String eventDescription;

    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private final List<Expense> expenses = new ArrayList<>();

    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private final List<Payment> payments = new ArrayList<>();
}
