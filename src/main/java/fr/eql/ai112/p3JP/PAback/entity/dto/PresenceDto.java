package fr.eql.ai112.p3JP.PAback.entity.dto;

public class PresenceDto {

    private Long userId;
    private Long eventId;

    public PresenceDto(Long userId, Long eventId) {
        this.userId = userId;
        this.eventId = eventId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Long getUserId() {
        return userId;
    }

    public Long getEventId() {
        return eventId;
    }
}
