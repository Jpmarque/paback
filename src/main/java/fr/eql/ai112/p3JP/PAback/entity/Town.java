package fr.eql.ai112.p3JP.PAback.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "town")
public class Town {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "town_id")
    private Long townId;

    private String city;

    @Column(name = "city_code")
    private String cityCode;


    @ManyToOne
    @JoinColumn(referencedColumnName = "country_id")
    private Country country;

    @OneToMany(mappedBy = "town", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private final List<Location> location = new ArrayList<>();
}
