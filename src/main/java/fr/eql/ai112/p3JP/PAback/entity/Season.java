package fr.eql.ai112.p3JP.PAback.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "season")
public class Season {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "season_id")
    private Long seasonId;

    @Column(name = "season_beginning")
    private LocalDate seasonBeginning;

    @Column(name = "season_end")
    private LocalDate seasonEnd;

    @OneToMany(mappedBy = "season", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private final List<RugbyMatch> rugbyMatches = new ArrayList<>();

    @OneToMany(mappedBy = "season", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private final List<Licence> licences = new ArrayList<>();
}
