package fr.eql.ai112.p3JP.PAback.security;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfigurer extends WebSecurityConfigurerAdapter {


    @Override @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Enable CORS and disable CSRF
        http = http.cors().and().csrf().disable();

        // Set session management to stateless
        http = http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and();

        // Set unauthorized requests exception handler
        http = http
                .exceptionHandling()
                .authenticationEntryPoint(new SecurityEntryPoint())
                .and();

        // Set permissions on endpoints
        http
                .authorizeRequests()
                // Our public endpoints
                .antMatchers("/authorize").permitAll()
                .antMatchers("/register").permitAll()
                .antMatchers("/admin").hasAnyRole("ADMIN")
                .antMatchers("/validate").hasAnyRole("ADMIN")
                .antMatchers("/touches").hasAnyRole("PLAYER", "ADMIN")
                .antMatchers("/backwards").hasAnyRole("PLAYER", "ADMIN")
                .antMatchers("/forwards").hasAnyRole("PLAYER", "ADMIN")
                // Our private endpoints
                .anyRequest().authenticated();

        http
                .addFilterBefore(securityFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public SecurityFilter securityFilter() {
        return new SecurityFilter();
    }

}
