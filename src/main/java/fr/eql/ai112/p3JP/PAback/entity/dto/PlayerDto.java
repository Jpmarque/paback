package fr.eql.ai112.p3JP.PAback.entity.dto;

public class PlayerDto {

    private Long userId;
    private String firstName;
    private String lastName;

    public PlayerDto(Long userId, String firstName, String lastName) {

        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Long getUserId() { return userId; }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
}
