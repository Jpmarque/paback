package fr.eql.ai112.p3JP.PAback.controller.rest;

import fr.eql.ai112.p3JP.PAback.entity.User;
import fr.eql.ai112.p3JP.PAback.entity.dto.MemberDto;
import fr.eql.ai112.p3JP.PAback.repository.MemberDao;
import fr.eql.ai112.p3JP.PAback.service.MemberService;
import fr.eql.ai112.p3JP.PAback.utils.UrlUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@CrossOrigin(origins = UrlUtils.FRONT_END_URL)
@RequestMapping("/members")
public class MemberRestController {

    MemberService memberService;

    @Autowired
    public void setMemberDao(MemberDao memberDao) {
        this.memberDao = memberDao;
    }

    MemberDao memberDao;

    @Autowired
    public void setMemberService(MemberService memberService) {
        this.memberService = memberService;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("")
    List<MemberDto> getAllMembers() {
        System.out.println("je suis dans le rest controller");
        return memberService.getAllMembers();
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/{id}")
    MemberDto getMember(@PathVariable long id) {
        return memberService.getMember(id);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/accept")
    public User acceptMember(@RequestBody String mail) {
        System.out.println(mail);
        User user = memberDao.getMemberByMail(mail.replace("\"",""));
        user.setDateOfValidation(LocalDate.now());
        return memberDao.save(user);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/refuse")
    public User refuseMember(@RequestBody String mail) {
        User user = memberDao.getMemberByMail(mail.replace("\"",""));
        user.setDateOfRefusal(LocalDate.now());
        return memberDao.save(user);
    }
}
