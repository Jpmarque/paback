package fr.eql.ai112.p3JP.PAback.entity.dto;

import java.time.LocalDate;

public class ThisSeasonMatchDto {

    private Long eventId;
    private Short pointScored;
    private Short pointTaken;
    private Byte bonusPoint;
    private LocalDate effectiveStartDate;
    private String team;

    public ThisSeasonMatchDto(Long eventId, Short pointScored, Short pointTaken, Byte bonusPoint, LocalDate effectiveStartDate, String team) {
        this.eventId = eventId;
        this.pointScored = pointScored;
        this.pointTaken = pointTaken;
        this.bonusPoint = bonusPoint;
        this.effectiveStartDate = effectiveStartDate;
        this.team = team;
    }

    public Long getEventId() { return eventId; }

    public Short getPointScored() {
        return pointScored;
    }

    public Short getPointTaken() {
        return pointTaken;
    }

    public Byte getBonusPoint() {
        return bonusPoint;
    }

    public LocalDate getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public String getTeam(){return team;}
}
