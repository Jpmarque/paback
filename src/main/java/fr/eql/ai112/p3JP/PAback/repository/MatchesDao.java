package fr.eql.ai112.p3JP.PAback.repository;


import fr.eql.ai112.p3JP.PAback.entity.RugbyMatch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface MatchesDao extends JpaRepository<RugbyMatch, Long> {

    String REQ_THIS_SEASON_MATCHES_ID = "SELECT rugby_match.event_id FROM rugby_match " +
            "INNER JOIN team ON team.team_id = rugby_match.team_team_id " +
            "INNER JOIN season ON season.season_id = rugby_match.season_season_id " +
            "INNER JOIN event ON event.event_id = rugby_match.event_id " +
            "WHERE season.season_id = (SELECT MAX(season.season_id) FROM season);" ;

    String REQ_ALL_IDS_OF_LICENCED_PLAYERS= "SELECT user_id FROM user " +
            "INNER JOIN licence ON licence.user_user_id = user.user_id " +
            "INNER JOIN season ON season.season_id = licence.season_season_id " +
            "WHERE season.season_id = (SELECT MAX(season.season_id) FROM season); ";

    /**
     * This list of ids gives only the one matching the last season in the table
     * @return list of ids (type: long)
     */
    @Query(value = REQ_ALL_IDS_OF_LICENCED_PLAYERS, nativeQuery = true)
    List<Long> getIdsOffLicencedPlayers();

    @Query(value = REQ_THIS_SEASON_MATCHES_ID, nativeQuery = true)
    List<Long> getIdsOffThisSeasonsMatches();

//    @Query(value = REQ_ALL_IDS_OF_LICENCED_PLAYERS, nativeQuery = true)
//    List<Long> getIdsOffLicencedPlayers();


    @Query("SELECT event.effectiveStartDate FROM Event event WHERE event.eventId = :id")
    LocalDate datesOfMAtches(@Param("id") long id);

    RugbyMatch findByEventId(long id);

    @Query("SELECT team.teamName FROM Team team INNER JOIN  RugbyMatch rugbymatch ON rugbymatch.team.teamId = team.teamId INNER JOIN Event event ON event.eventId=rugbymatch.eventId WHERE event.eventId = :id")
    String nameOfTeam(@Param("id") long id);
}
