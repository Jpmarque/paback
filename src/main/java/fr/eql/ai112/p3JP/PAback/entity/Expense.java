package fr.eql.ai112.p3JP.PAback.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "expense")
public class Expense {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "expense_id")
    private Long expenseId;

    @Column(name = "date_expense")
    private LocalDate dateExpense;

    private float amount;
    private String reason;

    @ManyToOne
    @JoinColumn(referencedColumnName = "event_id")
    private Event event;
}
