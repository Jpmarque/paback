package fr.eql.ai112.p3JP.PAback.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("M")
@Table(name = "rugby_match")
public class RugbyMatch extends Event{

    @Column(name = "point_scored")
    private Short pointScored;

    @Column(name = "point_taken")
    private Short pointTaken;

    @Column(name = "bonus_point")
    private Byte bonusPoint;

    @Column(name = "payment_referee")
    private Byte paymentReferee;

    @Column(name = "payment_field")
    private Byte paymentField;

    @ManyToOne
    @JoinColumn(referencedColumnName = "season_id")
    private Season season;

    public RugbyMatch(Byte bonusPoint, Short pointTaken, Short pointScored) {
        super();
    }

    public Short getPointScored() {
        return pointScored;
    }

    public Short getPointTaken() {
        return pointTaken;
    }

    public Byte getBonusPoint() {
        return bonusPoint;
    }

    @ManyToOne
    @JoinColumn(referencedColumnName = "team_id")
    private Team team;

    @ManyToMany
    @JoinTable( name = "presence",
                joinColumns = @JoinColumn(name = "event_id"),
                inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> people =new ArrayList<>();

    @ManyToMany
    @JoinTable( name = "previsionnal_presence",
            joinColumns = @JoinColumn(name = "event_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> peopleComing =new ArrayList<>();

    public RugbyMatch() {
    }

    public RugbyMatch(Short pointScored, Short pointTaken, Byte bonusPoint, Byte paymentReferee, Byte paymentField, Season season, Team team, List<User> people, User user) {
        this.pointScored = pointScored;
        this.pointTaken = pointTaken;
        this.bonusPoint = bonusPoint;
        this.paymentReferee = paymentReferee;
        this.paymentField = paymentField;
        this.season = season;
        this.team = team;
        this.people = people;
        this.user = user;
    }

    @ManyToOne
    @JoinColumn(referencedColumnName = "user_id")
    private User user;
}
