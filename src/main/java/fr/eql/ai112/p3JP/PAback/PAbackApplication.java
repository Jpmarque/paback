package fr.eql.ai112.p3JP.PAback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PAbackApplication {

	public static void main(String[] args) {
		SpringApplication.run(PAbackApplication.class, args);
	}

}
