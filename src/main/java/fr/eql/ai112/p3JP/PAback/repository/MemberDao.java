package fr.eql.ai112.p3JP.PAback.repository;

import fr.eql.ai112.p3JP.PAback.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.time.LocalDate;

public interface MemberDao extends JpaRepository<User, Long> {

    int numberOfUser = 0;
        String REQ_COUNT_SEASON_MATCH = "SELECT COUNT(*) FROM `presence` " +
    "INNER JOIN user ON user.user_id = presence.user_id " +
    "INNER JOIN rugby_match ON rugby_match.event_id = presence.event_id " +
    "INNER JOIN season ON season.season_id = rugby_match.season_season_id " +
    "WHERE user.user_id = :id " +
    "AND season.season_id = (SELECT MAX(season.season_id) FROM season); ";


    String REQ_LAST_LICENCE_DATE = "SELECT written_date FROM `licence` " +
            "INNER JOIN user ON user.user_id = licence.user_user_id " +
            "INNER JOIN season ON season.season_id = licence.season_season_id " +
            "WHERE user.user_id = :id " +
            "AND season.season_id = (SELECT MAX(season.season_id) FROM season);";

    @Query(value = REQ_COUNT_SEASON_MATCH, nativeQuery = true)
    byte gamesPlayedThisSeason(@Param("id") long id);

    @Query(value = REQ_LAST_LICENCE_DATE, nativeQuery = true)
    LocalDate thisYearLicenceDate(@Param("id") long id);

    User findByUserId(long id);  //The role is included in the user due to the eager connection

    @Query("SELECT COUNT (user.userId) FROM User user")
    long numberOfUser();

    @Query("SELECT user FROM User user WHERE user.mail = :mail")
    User getMemberByMail(@Param("mail") String mail);

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO presence (user_id, event_id) VALUES (:user_id, :event_id); ", nativeQuery = true)
    void saveParticipation(@Param("user_id") long user_id, @Param("event_id") long event_id);
}
