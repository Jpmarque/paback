package fr.eql.ai112.p3JP.PAback.service;

import fr.eql.ai112.p3JP.PAback.entity.dto.MemberDto;

import java.util.List;

public interface MemberService {

    MemberDto getMember(long id);

    List<MemberDto> getAllMembers();



}
