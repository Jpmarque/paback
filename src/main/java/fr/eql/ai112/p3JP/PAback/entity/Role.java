package fr.eql.ai112.p3JP.PAback.entity;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "role")
public class Role implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private Long roleId;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    private String role;

    @ManyToMany
    @JoinTable(name = "Role_Person",
            joinColumns = @JoinColumn(name="roleId"),
            inverseJoinColumns = @JoinColumn(name = "user_id" ))
    private List<User> people = new ArrayList<>();


    @Override
    public String getAuthority() {
        return role;
    }
}
