package fr.eql.ai112.p3JP.PAback.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Payment {

    @Id
    private Long payment_id;

    @Column(name = "date_reception_payment")
    private LocalDate dateReceptionPayment;

    private double amount;

    @ManyToOne
    @JoinColumn(referencedColumnName = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(referencedColumnName = "event_id")
    private Event event;



}
