package fr.eql.ai112.p3JP.PAback.controller.rest;

import fr.eql.ai112.p3JP.PAback.entity.dto.AuthRequest;
import fr.eql.ai112.p3JP.PAback.entity.dto.AuthResponse;
import fr.eql.ai112.p3JP.PAback.exception.AccountExistsException;
import fr.eql.ai112.p3JP.PAback.exception.UnauthorizedException;
import fr.eql.ai112.p3JP.PAback.service.JwtUserService;
import fr.eql.ai112.p3JP.PAback.utils.UrlUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = UrlUtils.FRONT_END_URL)
@RequestMapping
public class SecurityController {

    @Autowired
    private JwtUserService userService;

    @PostMapping("/register")
    public ResponseEntity<AuthResponse> register(@RequestBody AuthRequest requestDto) throws AccountExistsException {
        UserDetails user = userService.save(requestDto.getUsername(), requestDto.getPassword(), requestDto.getBirthdate(),requestDto.getFirstName(), requestDto.getLastName(), requestDto.getPhoneNumber());
        String token = userService.generateJwtForUser(user);
        System.out.println("TOKEN BORDEL : " + token);
        System.out.println("USER BORDEL : " + user.getUsername() + " ||| " + user.getPassword());
        return ResponseEntity.ok(new AuthResponse(user, token));
    }

    @PostMapping("/authorize")
    public ResponseEntity<AuthResponse> authorize(@RequestBody AuthRequest requestDto) throws UnauthorizedException {
        Authentication authentication = null;
        try {
            authentication = userService.authenticate(requestDto.getUsername(), requestDto.getPassword());
            SecurityContextHolder.getContext().setAuthentication(authentication);

            // Token generation
            UserDetails user = (UserDetails) authentication.getPrincipal();
            String token = userService.generateJwtForUser(user);
            System.out.println(token);
            return ResponseEntity.ok(new AuthResponse(user, token));

        } catch(AuthenticationException e) {
            throw new UnauthorizedException();
        }
    }
}
