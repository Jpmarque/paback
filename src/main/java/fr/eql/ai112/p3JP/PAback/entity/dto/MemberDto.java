package fr.eql.ai112.p3JP.PAback.entity.dto;

import fr.eql.ai112.p3JP.PAback.entity.Payment;
import fr.eql.ai112.p3JP.PAback.entity.Role;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MemberDto {

    private List<Role> roles = new ArrayList<>();
    private String mail;
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private LocalDate birthdate;
    private LocalDate validationDate;
    private LocalDate refusalDate;
    private String adress;
    private Byte gamesPlayedThisSeason;
    private LocalDate dateMedicalCertificate;
    private  LocalDate thisYearLicenceDate;

    public MemberDto(List<Role> roles, String mail, String password, String firstName, String lastName, String phoneNumber, LocalDate birthdate, LocalDate validationDate, LocalDate refusalDate, String adress, Byte gamesPlayedThisSeason, LocalDate dateMedicalCertificate, LocalDate thisYearLicenceDate) {
        this.roles = roles;
        this.mail = mail;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.birthdate = birthdate;
        this.validationDate = validationDate;
        this.refusalDate = refusalDate;
        this.adress = adress;
        this.gamesPlayedThisSeason = gamesPlayedThisSeason;
        this.dateMedicalCertificate = dateMedicalCertificate;
        this.thisYearLicenceDate = thisYearLicenceDate;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public String getMail() {
        return mail;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public LocalDate getValidationDate() {
        return validationDate;
    }

    public LocalDate getRefusalDate() {
        return refusalDate;
    }

        public String getAdress() {
        return adress;
    }

    public Byte getGamesPlayedThisSeason() {
        return gamesPlayedThisSeason;
    }

    public LocalDate getDateMedicalCertificate() {
        return dateMedicalCertificate;
    }

    public LocalDate getThisYearLicenceDate() {
        return thisYearLicenceDate;
    }
}
