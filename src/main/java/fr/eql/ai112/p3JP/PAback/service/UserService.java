package fr.eql.ai112.p3JP.PAback.service;

import fr.eql.ai112.p3JP.PAback.exception.AccountExistsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.time.LocalDate;

public interface UserService extends UserDetailsService {

    Authentication authenticate(String username, String password) throws AuthenticationException;
    UserDetails save(String username, String password, LocalDate birthdate, String firstName, String lastName, String phoneNumber) throws AccountExistsException;


}
