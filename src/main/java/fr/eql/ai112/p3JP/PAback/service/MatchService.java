package fr.eql.ai112.p3JP.PAback.service;

import fr.eql.ai112.p3JP.PAback.entity.RugbyMatch;
import fr.eql.ai112.p3JP.PAback.entity.User;
import fr.eql.ai112.p3JP.PAback.entity.dto.PlayerDto;
import fr.eql.ai112.p3JP.PAback.entity.dto.PresenceDto;
import fr.eql.ai112.p3JP.PAback.entity.dto.ThisSeasonMatchDto;

import java.util.List;


public interface MatchService {

    List<PlayerDto> getALLPlayerLicenced();

    User findByUserId(Long id);

    RugbyMatch findByEventId(Long id);



    List<ThisSeasonMatchDto>
    matchesThisSeason();


    User save(User user);

     void saveParticipation(PresenceDto presenceDto);
}
