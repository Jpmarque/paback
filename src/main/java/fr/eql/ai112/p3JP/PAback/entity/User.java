package fr.eql.ai112.p3JP.PAback.entity;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "user")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    private LocalDate birthdate;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(unique = true, length = 40)
    private String mail;

    private String password;
    private String adress;

    @Column(name = "date_of_certificate")
    private LocalDate dateOfCertificate;

    @Column(name = "date_of_validation")
    private LocalDate dateOfValidation;

    @Column(name = "date_of_refusal")
    private LocalDate dateOfRefusal;

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name = "Role_Person",
            joinColumns = @JoinColumn(name="user_id"),
            inverseJoinColumns = @JoinColumn(name = "roleId" ))
    private Collection<Role> roles;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private final List<Licence> licences = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private final List<Payment> payments = new ArrayList<>();

    @ManyToOne
    @JoinColumn(referencedColumnName = "town_id")
    private Town town;

    @ManyToMany
    @JoinTable( name = "presence",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "event_id"))
    private List<RugbyMatch> rugbyPresence =new ArrayList<>();

    @ManyToMany
    @JoinTable( name = "previsionnal_presence",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "event_id"))
    private List<RugbyMatch> rugbyPrevisionnalPresence =new ArrayList<>();

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return mail;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setDateOfCertificate(LocalDate dateOfCertificate) {
        this.dateOfCertificate = dateOfCertificate;
    }

    public void setDateOfValidation(LocalDate dateOfValidation) {
        this.dateOfValidation = dateOfValidation;
    }

    public void setDateOfRefusal(LocalDate dateOfRefusal) {
        this.dateOfRefusal = dateOfRefusal;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public void setTown(Town town) {
        this.town = town;
    }

    public void addRugbyPresence(RugbyMatch rugbyPresence) {
        this.rugbyPresence.add(rugbyPresence);
    }

    public Long getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getMail() {
        return mail;
    }

    public LocalDate getDateOfCertificate() {
        return dateOfCertificate;
    }

    public LocalDate getDateOfValidation() {
        return dateOfValidation;
    }

    public LocalDate getDateOfRefusal() {
        return dateOfRefusal;
    }

    public Town getTown() {
        return town;
    }

}