package fr.eql.ai112.p3JP.PAback.repository;

import fr.eql.ai112.p3JP.PAback.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserDao extends JpaRepository<User, Long> {
    User findByMail(String Login);

    @Query("SELECT user FROM User user WHERE user.mail= :mail AND user.password = :password")
    User findByMailAndPassword(@Param("mail") String login, @Param("password") String password);

}
