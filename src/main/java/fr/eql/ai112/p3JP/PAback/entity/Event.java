package fr.eql.ai112.p3JP.PAback.entity;

import javax.persistence.*;
import java.time.LocalDate;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="event_type")
public abstract class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_id")
    private Long eventId;

    @Column(name = "previsionnal_start_date")
    private LocalDate previsionnalStartDate;

    @Column(name = "effective_start_date")
    private LocalDate effectiveStartDate;

    @ManyToOne
    @JoinColumn(referencedColumnName = "location_id")
    private Location location;

    public Long getEventId() {
        return eventId;
    }
}
