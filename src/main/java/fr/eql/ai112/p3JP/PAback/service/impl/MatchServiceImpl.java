package fr.eql.ai112.p3JP.PAback.service.impl;

import fr.eql.ai112.p3JP.PAback.entity.RugbyMatch;
import fr.eql.ai112.p3JP.PAback.entity.User;
import fr.eql.ai112.p3JP.PAback.entity.dto.PlayerDto;
import fr.eql.ai112.p3JP.PAback.entity.dto.PresenceDto;
import fr.eql.ai112.p3JP.PAback.entity.dto.ThisSeasonMatchDto;
import fr.eql.ai112.p3JP.PAback.repository.MatchesDao;
import fr.eql.ai112.p3JP.PAback.repository.MemberDao;
import fr.eql.ai112.p3JP.PAback.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Service
public class MatchServiceImpl implements MatchService{


    MemberDao memberDao;
    MatchesDao matchesDao;

    @Autowired
    public void setMatchesDao(MatchesDao matchesDao) {
        this.matchesDao = matchesDao;
    }

    @Autowired
    public void setMemberDao(MemberDao memberDao) {
        this.memberDao = memberDao;
    }

    @Override
    public List<PlayerDto> getALLPlayerLicenced() {
        List<Long> getIdsOffLicencedPlayers = matchesDao.getIdsOffLicencedPlayers();
        List<PlayerDto> getALLPlayerLicenced = new ArrayList<>();
        for (Long id : getIdsOffLicencedPlayers) {
            User findByUserId = memberDao.findByUserId(id);
            PlayerDto playerDto = new PlayerDto(findByUserId.getUserId(), findByUserId.getFirstName(), findByUserId.getLastName());
            getALLPlayerLicenced.add(playerDto);
        }
        return getALLPlayerLicenced;
    }

    @Override
    public User findByUserId(Long id) {
        return memberDao.findByUserId(id);
    }
    @Override
    public RugbyMatch findByEventId(Long id){
        return matchesDao.findByEventId(id);
    }

    @Override
    public User save(User user) {
        return memberDao.save(user);
    }

    @Override
    public void saveParticipation(PresenceDto presenceDto) {
        long eventId = presenceDto.getEventId();
        long userId = presenceDto.getUserId();
          memberDao.saveParticipation(userId,eventId);
    }

    @Override
    public List<ThisSeasonMatchDto> matchesThisSeason() {
        List<Long> getIdsOfThisSeasonsMatches = matchesDao.getIdsOffThisSeasonsMatches();
        List<ThisSeasonMatchDto> matchesThisSeason = new ArrayList<>();
        for (Long id : getIdsOfThisSeasonsMatches) {
            RugbyMatch findByEventId= matchesDao.findByEventId(id);
            LocalDate dateOfMatch = matchesDao.datesOfMAtches(id);
            String nameOfTeam = matchesDao.nameOfTeam(id);
            ThisSeasonMatchDto thisSeasonMatchDto = new ThisSeasonMatchDto(id, findByEventId.getPointScored(),
                    findByEventId.getPointTaken(), findByEventId.getBonusPoint(),dateOfMatch, nameOfTeam );
            matchesThisSeason.add(thisSeasonMatchDto);
        }
        return matchesThisSeason;
    }
}