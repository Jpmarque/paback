package fr.eql.ai112.p3JP.PAback.controller.rest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class MatchRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MatchRestController matchRestController;

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void testGetALLPlayerLicenced() throws Exception{
        mockMvc.perform(get("/matches")).andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    void testMatchesThisSeason() throws Exception{
        mockMvc.perform(get("/matches/players")).andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = {"USER"})
    void testValidatePlayer() throws Exception{
        mockMvc.perform(post("/matches/validate")).andExpect(status().is4xxClientError());
    }
}
