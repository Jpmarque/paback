package fr.eql.ai112.p3JP.PAback.security.repository;


import fr.eql.ai112.p3JP.PAback.repository.MemberDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import javax.persistence.EntityManager;
import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.*;

//database.replace=NONE permit a rollback after each test to insure integrity of database
@DataJpaTest(properties = {
        "spring.test.database.replace=NONE "
})
class MemberDaoTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private MemberDao memberDao;

    @Test
    void contextLoads() {
        assertNotNull(entityManager);
        assertNotNull(dataSource);
    }

    @Test
    void gamesPlayedThisSeason() {
        assertNotNull(memberDao.gamesPlayedThisSeason(1L));
    }

    @Test
    void thisYearLicenceDate(){
        assertNotNull(memberDao.thisYearLicenceDate(1L));
    }

//this test will break if you validate player with id 1 another time
    @Test
    void saveParticipation(){
      memberDao.saveParticipation(1L,2L);
        assertEquals(1, memberDao.gamesPlayedThisSeason(1L));
    }
//in normal conditions of use, this test should always run smoothly
    @Test
    void saveParticipationMissed(){
        memberDao.saveParticipation(1L,1L);
        assertEquals(0,memberDao.gamesPlayedThisSeason(2L));
    }
}